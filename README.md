# Weather Prediction App

This project is developed to predict the weather temperature of the next 5 days along with the actual season (e.g. sunny, rainy, etc.).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software:

* [Git](https://git-scm.com/)
* [Node.js](https://nodejs.org/en/)
* [React.js](https://reactjs.org/)
* [Electron.js](https://www.electronjs.org/)
* [node-sass](https://www.npmjs.com/package/node-sass)
* [Moment.js](https://momentjs.com/)

### Installing

Here's a step by step series of examples that tell you how to get a development env running:

In your terminal, clone the repository

```
git clone git clone https://dgabrielablay@bitbucket.org/dgabrielablay/weather-prediction-app.git
```

Go into that directory

```
cd Weather-Prediction-App
```

Install dependencies

```
npm install
```

Run the project

```
npm run electron-dev
```

## Deployment

Should you need to deploy this project in a live system, this command will generate a .dmg file if you are in macOS or a .exe file if you are in a Windows pc.

```
npm run electron-pack
```

## Built With

* [React.js](https://reactjs.org/) - Frontend client used
* [Electron.js](https://www.electronjs.org/) - Generating .exe or .dmg framework used

## Versioning

We use [Git](https://git-scm.com/) for versioning.

## Authors

* **Don Gabriel Ablay** - *Initial work* - [dgda](https://github.com/dgda)

## Acknowledgments

* Hat tip to [Gary](https://www.youtube.com/channel/UCVyRiMvfUNMA1UPlDPzG5Ow) for the tutorial on how to develop applications using sass

import React from 'react';
import './assets/css/owfont.min.css';
import './App.scss';
let moment = require('moment');

function App() {
  let [forecast, setForecast] = React.useState(null);
  let [firstForecast, setFirstForecast] = React.useState(null);
  document.title = 'CS0017 Final Project';
  const apiKey = 'a477d9a9d689d50a0c801cc4bec55295';
  const currentLocation = 'Manila,ph';
  const weatherApi = `http://api.openweathermap.org/data/2.5/forecast?q=${currentLocation}&units=metric&APPID=${apiKey}`;

  let getForecast = async() => {
    console.log('attempting handshake with api')
    await fetch(weatherApi)
      .then(response => response.json())
      .then(data => {
        const dailyData = data.list.filter(
          reading => reading.dt_txt.includes('06:00:00')
        );
        let firstForecast = dailyData.shift();
        console.log(dailyData);
        setFirstForecast(firstForecast);
        setForecast(dailyData);
        console.log('successful handshake with api');
      }).catch(error => {
        console.log(error);
      })
  }

  if(forecast === null ||
     firstForecast === null ||
     firstForecast.weather === null
  ){
    getForecast();
    return (<p>Loading data...</p>);
  }
  const imgUrl = `owf owf-${firstForecast.weather[0].id} owf-5x`;
  let newDate = new Date();
  const weekday = firstForecast.dt * 1000;
  newDate.setTime(weekday);
  let bgClass = '';
  if(firstForecast.weather[0].main === 'Clear'){
    bgClass = 'bg-clear';
  }else{
    bgClass = 'bg-cloudy';
  }
  return (
    <div>
      <div id="bg" className={bgClass}></div>

      <header>
        <a href="#"></a>
      </header>

      <main>
        <section className="main_card">
          <i className={imgUrl}></i>
          <h1>
            Tomorrow&nbsp;
            <small>{moment(newDate).format('MMMM Do')}</small>
          </h1>
          <h2>{Math.round(firstForecast.main.temp)} °C <span className='weather-description'>( {firstForecast.weather[0].description} )</span></h2>
        </section>

        <section id="deck">
          {forecast.map((reading, index) => {
            let newDate = new Date();
            const weekday = reading.dt * 1000;
            newDate.setTime(weekday);

            const imgUrl = `owf owf-${reading.weather[0].id} owf-5x`;
            return (
              <section className="card" key={index}>
                <i className={imgUrl}></i>
                <h3>
                  {moment(newDate).format('dddd')}<br/>
                  <small>{moment(newDate).format('MMMM Do')}</small>
                </h3>
                <h2>{Math.round(reading.main.temp)} °C <span className='weather-description'>( {reading.weather[0].description} )</span></h2>
              </section>
            )
          })}
        </section>

        <section id="primary">
          <h1>your personal weather app</h1>
          <p>Get weather forecasting of your current location. ({currentLocation})</p>
        </section>
      </main>
    </div>
  );
}

export default App;
